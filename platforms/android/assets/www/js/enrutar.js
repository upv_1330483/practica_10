var enrutar = (function () {

    "use strict";

    var rutas = [];

    function agregaRuta(ruta, manejador) {

        rutas.push({partes: ruta.split('/'), manejador: manejador});
    }

    function carga(ruta) {
        window.location.hash = ruta;
    }

    function inicia() {

        var path = window.location.hash.substr(1),
            partes = path.split('/'),
            longitudPartes = partes.length;

        for (var i = 0; i < rutas.length; i++) {
            var ruta = rutas[i];
            if (ruta.partes.length === longitudPartes) {
                var params = [];
                for (var j = 0; j < longitudPartes; j++) {
                    if (ruta.partes[j].substr(0, 1) === ':') {
                        params.push(partes[j]);
                    } else if (ruta.partes[j] !== partes[j]) {
                        break;
                    }
                }
                if (j === longitudPartes) {
                    ruta.manejador.apply(undefined, params);
                    return;
                }
            }
        }
    }

    window.onhashchange = inicia;

    return {
        agregaRuta: agregaRuta,
        carga: carga,
        inicia: inicia
    };

}());